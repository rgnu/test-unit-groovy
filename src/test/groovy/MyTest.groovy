import groovy.util.GroovyTestCase

class MyTest extends GroovyTestCase {

    void testSomething() {
        assert 1 == 1
        assert 2 + 2 == 4 : "We're in trouble, arithmetic is broken"
    }

    void testFoo() {
        assert 1 == 1
        assert 3 + 2 == 5 : "We're in trouble, arithmetic is broken"
    }

}
