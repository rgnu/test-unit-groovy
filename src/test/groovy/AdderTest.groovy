package test

import groovy.util.GroovyTestCase
import test.Adder

class AdderTest extends GroovyTestCase {

    Adder obj

    void setUp() {
        println "Running Setup"
        this.obj = new Adder()
    }

    void testAdd() {
        println "Running testAdd"
        assert obj.add(2, 1) == 3
    }

    void testMax() {
        println "Running testMax"
        assert obj.max(2, 1) == 2
    }
}
