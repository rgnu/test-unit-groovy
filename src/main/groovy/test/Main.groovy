package test

/**
 *
 * @author dev
 */
class Main {
    static void main(String[] args) {
        println "Hello World!!!!!!!"
        println System.properties.get('log4j.configuration')

        def config = new Properties()
        new File('config/glue.properties').withInputStream { stream -> config.load(stream)}
        println config
    }
}

